/*
    Lista apenas os pokemons com menos de 250 pontos de habilidade (total)
    A lista deve estar ordenada do menos habilidoso para o maior
*/

var { pokemon } = require('../data')

module.exports = function (req, res) {

    // TODO: fragile pokemon list"
    var result = [];
    var fragile = [];
    //Implementação
    pokemon.forEach(mon => {
        if (mon.total < 250) {
            fragile.push(mon);
        }
    });

    fragile.sort(reordenacao);
    console.log('fragile unordened', fragile);

    //Retorno
    // res.json(result)
}

var reordenacao = function compara(a, b) {
    if (a.total > b.total) {
        return 1;
    } else if (a.total < b.total) {
        return -1;
    } else {
        return 0;
    }
}