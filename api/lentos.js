/*
    Lista apenas os pokemons com menos de 40 pontos de velocidade (speed)
    A lista deve estar ordenada do mais lento para o mais rápido
*/

var { pokemon } = require('../data')

module.exports = function (req, res) {

    // TODO: slow pokemon list"
    var result = [];
    //Implementação
    pokemon.forEach(mon => {
        if (mon.speed < 40) {
            result.push(mon);
        }
    });
    console.log('slow', result);

    //Retorno
    res.json(result);
}