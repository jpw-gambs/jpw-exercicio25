/*
    Lista apenas os pokemons o mesmo valor de ataque e defesa
*/

var { pokemon } = require('../data')

module.exports = function (req, res) {

    // TODO: balanced pokemon list"
    var result = [];
    //Implementação
    pokemon.forEach(mon => {
        if (mon.attack == mon.defense) {
            result.push(mon);
        }
    });
    console.log('balanced', result);

    //Retorno
    res.json(result)
}