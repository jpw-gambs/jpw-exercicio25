/*
    Lista um pokémon aleatorio da lista
*/

var { pokemon } = require('../data')

module.exports = function (req, res) {

    var result = "random pokemon"

    //Implementação

    var min = Math.ceil(0);
    var max = Math.floor(pokemon.length);
    var number = (Math.floor(Math.random() * (max - min)) + min);

    result = pokemon[number];

    console.log(result)

    //Retorno
    res.json(result)
}